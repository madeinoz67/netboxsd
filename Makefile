PROJECT_NAME := "netboxsd"
BINDIR ?= "$(CURDIR)/bin"
GITHASH = $(shell git describe --always --long --dirty)
VERSION := $(shell cat VERSION)
PKG_BASE := "gitlab.com/madeinoz67/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG_BASE}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
	@go test -short ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}

fmt: ## Format Go files
	@gofmt -s -w ${GO_FILES}

files: ## List all Go files
	@echo ${GO_FILES}

coverage: ## Generate global code coverage report
	./coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./coverage.sh html;

dep: ## Get the dependencies
	@go get -v -d ./...
	@go get -u golang.org/x/lint/golint

build: dep ## Build the binary file
	@go build -v -o "$(BINDIR)/netboxsd" -ldflags="-s -w -X main.version=${VERSION} -X main.githash=${GITHASH}" $(PKG_BASE)

clean: ## Clean all sub-targets
	@rm -rf "${BINDIR}"
	@rm -rf coverage

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
